<?php
/*
 * Class to find total no of wine sold
 * and list of person geting the wine
 */
class personwine {

    public static $txtFile = 'person_wine_3.txt'; // txt file path

    public function loadData() {
        $line_of_text = "";
        $wine_list = array();
        $total_wine_sold = 0;
        $person_wine_list = array();

        /*
         * Looop to get txt file data in array
         */
        $file = fopen(self::$txtFile, "r");
        while (($line_of_text = fgets($file)) !== false) {
            $person_wine_data = explode("\t", $line_of_text);
            $person_name = trim($person_wine_data[0]);
            $wine_name = trim($person_wine_data[1]);
            if (!array_key_exists($wine_name, $wine_list)) {
                $wine_list[$wine_name] = [];
            }
            $wine_list[$wine_name][] = $person_name;
            $all_wines[] = $wine_name;
        }
        fclose($file);
        
        // unique wine list
        $all_wines = array_unique($all_wines); 

        foreach ($all_wines as $key => $val) {
            $counter = 0;
            // loop for 10 wine each person happy to buy
            while ($counter < 10) {         
                if (!isset($wine_list[$val][0])) {
                    $wine_list[$val][0] = null;
                }

                $person = $wine_list[$val][0];
                if (!array_key_exists($person, $person_wine_list)) {
                    $person_wine_list[$person] = [];
                }
                // since 3 is allowed to each person
                if (count($person_wine_list[$person]) < 3) { 
                    $person_wine_list[$person][] = $val;
                    $total_wine_sold++;
                    break;
                }
                $counter++;
            }
        }

        /*
         * code to write txt file with the list of items.
         */
		 
		echo "Total wine sold: " . $total_wine_sold . "<br>";
		 
        $fh = fopen("output.txt", "w");
        fwrite($fh, "Total wine sold: " . $total_wine_sold . "\n");
        foreach (array_keys($person_wine_list) as $key => $val) {
            foreach ($person_wine_list[$val] as $key_1 => $val_1) {
				//echo $val . " " . $val_1 . "<br>";
                fwrite($fh, $val . " " . $val_1 . "\n");
            }
        }
        fclose($fh);
    }

}

$obj = new personwine();
$data = $obj->loadData();

?>
